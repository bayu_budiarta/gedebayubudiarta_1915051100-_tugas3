import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
      MaterialApp(
          home : Scaffold (
            backgroundColor: Colors.white,
            appBar: AppBar(
              title: Text ('Aplikasi Bayu'),
              backgroundColor: Colors.black,
              leading: new IconButton(icon: new Icon(Icons.apps, color: Colors.white)),
              actions: <Widget> [
                new IconButton(icon: new Icon(Icons.add, color: Colors.white)),
                new IconButton(icon: new Icon(Icons.search, color: Colors.white)),
              ],
            ),
            body: Center(
              child: Column(
                  children: <Widget>[
                    Image.network('https://i.ibb.co/SPsq0gm/IMG-20210317-151943.jpg',
                      width: 400.0,
                      height: 600.0,
                      fit: BoxFit.cover,
                    ),
                    Text('Gede Bayu Budi Arta',
                      style: TextStyle(
                          fontFamily: 'NunitoSemiBold'
                      ),
                    ),
                    Text('1915051100',
                      style: TextStyle(
                          fontFamily: 'NunitoSemiBold'
                      ),
                    ),
                  ]
              ),
            ),
          )

      )
  );
}